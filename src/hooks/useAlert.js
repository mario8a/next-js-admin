const { useState } = require("react");

const useAlert = (options) => {
  const defaultOpions = {
    active: false,
    message: "",
    type: "",
    autoClose: true
  };

  const [alert, setAlert] = useState({...defaultOpions, ...options});

  const toogleAlert = () => {
    setAlert(!alert.active);
  }

  return {
    alert,
    setAlert,
    toogleAlert
  }
}

export default useAlert;